#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, basic_collatz, check_cache

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    # Does this need to be corrected
    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)
        
    #my usertests
    def test_eval_5(self):
        v = collatz_eval(9000, 9999)
        self.assertEqual(v, 260)
    def test_eval_6(self):
        v = collatz_eval(1, 3000)
        self.assertEqual(v, 217)
    def test_eval_7(self):
        v = collatz_eval(9560, 9999)
        self.assertEqual(v, 242)
    # ----
    # basic
    # ----
    def test_basic_1(self):
        v = basic_collatz(1, 2)
        self.assertEqual(v, 2)
    def test_basic_2(self):
        v = basic_collatz(1, 10)
        self.assertEqual(v, 20)
    def test_basic_3(self):
        v = basic_collatz(1, 1000)
        self.assertEqual(v, 179)
        
    # -----
    # check_cache
    # -----
    def test_cache1(self):
        v = check_cache(1, 1000)
        self.assertEqual(v, 179)
    def test_cache2(self):
        v = check_cache(2001, 3000)
        self.assertEqual(v, 217)
    def test_cache3(self):
        v = check_cache(10001, 11000)
        self.assertEqual(v, 268)

    # -----
    # print
    # -----

    def test_print1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
    def test_print2(self):
        w = StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertEqual(w.getvalue(), "100 200 125\n")

    # -----
    # solve
    # -----

    def test_solve1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
    def test_solve2(self):
        r = StringIO("9000 9999\n1 3000\n9560 9999\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "9000 9999 260\n1 3000 217\n9560 9999 242\n")
    def test_solve3(self):
        r = StringIO("1 999999\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 999999 525\n")
                  

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
